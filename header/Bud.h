#if !defined(__Bud_H)
#define __Bud_H 

#include "ListTool2B.h"
#include "timer4.h"
#include <fstream>

using namespace std; 

class Bud : public NumElement {
private:
	int bud;
	char* budBrukernavn;
	int budTid;

public:
	Bud();
	Bud(int d, char* b);
	Bud(int d, ifstream & inn);
	int hentBud();
	char* hentBrukernavn();
	void settBudTid(int d);
	void skrivTilFil(ofstream & ut);

};


#endif