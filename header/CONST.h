//
//  CONST.h
//  ooprogprosjekt
//
//  Created by Simen Bjerkeset Andersen on 22/03/16.
//  Copyright © 2016 Simen Bjerkeset Andersen. All rights reserved.
//

#ifndef CONST_h
#define CONST_h

const int NVNLEN    = 80;
const int STRLEN    = 255;
const int STARTGJNR = 100;
const int MAXBUD    = 999999;
const int MAXPRIS   = 999999;
const int MAXPORTO  = 999;
const int MAXOKNING = 999999;


#endif /* CONST_h */
