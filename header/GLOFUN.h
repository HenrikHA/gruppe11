//
//  GLOFUN.h
//  ooprogprosjekt
//
//  Created by Simen Bjerkeset Andersen on 22/03/16.
//  Copyright © 2016 Simen Bjerkeset Andersen. All rights reserved.
//

#ifndef GLOFUN_h
#define GLOFUN_h

#include "Kunde.h"
#include "Kategori.h"
#include "Gjenstand.h"

void test();	// Skal fjernes før levering

void nyKunde();

void loggInn();

void loggInnBruker();

void loggUt();

void oversiktKjopSalg();

void betale();

void feedback();

void endreRettigheter();

void nyHovedkategori();

void nyUnderkategori();

void oversiktAuksjoner();

void oversiktGjenstander();

void oversiktDetaljer();

void nyGjenstand();

void nyttBud();

void avslutt();

int lesTall(const char t[], const int min, const int max);

char lesKommando(char t[]);

void lesTekst(const char t[], char s[], const int LEN);

void skrivTilFil();

void skrivGjenstandTilFil();

void lesFraFil();

void lesGjenstandFraFil();

Kunde* finnesKunde(char* bNavn);

void skrivHoverdkategorier();

void skrivUnderkategorier();

void skrivGjestMeny();

void skrivBrukerMeny();

void skrivAdminMeny();

void skrivGjestAuksjonUnderMeny();

void skrivBrukerAuksjonUnderMeny();

void kundeMain();

void auksjonMain();

void hentFilnavn(char s[]);

void hentFilnavnKjoper(char s[], const char b[]);

void hentFilnavnSelger(char s[], const char b[]);

int hentTid();

int hentTid(int d, int m, int y, int t, int mi);

void displayTid(int d);

Kategori* finnesKategori(char* nvn);

Kategori* finnesKategori(char* nvn, Kategori* k);

Gjenstand* finnGjenstand(int id);

Gjenstand* velgGjenstand(char s[]);

void oppdater();

void sokFil(fstream & fil, int id, char s[], char v, char t[]);

#endif /* GLOFUN_h */
