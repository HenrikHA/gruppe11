#if !defined(__Gjenstand_H)
#define __Gjenstand_H

#include "ListTool2B.h"
#include "timer4.h"
#include <fstream>

using namespace std;

class Gjenstand : public NumElement {
private:
	int gjenstandNr;
	char* brukernavnSelger;
	char* gjenstandTittel;
	char* beskrivelse;
	int tidspunktStart;
	int tidspunktSlutt;
	int startPris;
	int gjeldenePris;
	int budOkning;
	int porto;
	int aktiv;
	List* budListe;


public:
	Gjenstand();
	Gjenstand(int id, char* selger);
	Gjenstand(int id, ifstream & inn);
	int hentGjenstandNr();
	char* hentTittel();
	char* hentSelger();
	int hentTidspunktStart();
	int hentTidspunktSlutt();
	void settTidspunktStart(int d);
	void settTidspunktSlutt(int d);
	int hentStartPris();
	int hentBudOkning();
	void display();
	int hentAktiv();
	void settAktiv(int d);
	List* hentBud();
	void skrivTilFil(ofstream & ut);
};
#endif