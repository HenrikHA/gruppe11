#if !defined(__KATEGORI_H)
#define __KATEGORI_H 

#include "ListTool2B.h"
#include "timer4.h"
#include <fstream>

using namespace std;

class Kategori : public TextElement {
private:

	char* navn;
	int nr;
	int hovedKat;
	List* underkategorier;

public:

	Kategori();
    Kategori(char* nvn, int kat);
	Kategori(char* nvn, ifstream & inn);
	Kategori(char* nvn, int underKat, ifstream & inn);
	~Kategori();

	char* hentNavn();
	int hentNr();
	int hentHovedKat();
	List* hentUnderkategorier();
	void skrivTilFil(ofstream & ut);
    void leggeTilUnderkategori(Kategori* kat);

};


#endif