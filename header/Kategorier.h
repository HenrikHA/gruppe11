#if !defined(__KATEGORIER_H)
#define __KATEGORIER_H 

#include "ListTool2B.h"
#include "timer4.h"
#include "Kategori.h"

using namespace std;

class Kategorier {
private:

	List* kategorier = new List(Sorted);
	Kategori* aktivUnderkategori;


public:

	Kategorier();
	~Kategorier();
    
    List* hentKategorier();
    Kategori* hentHovedkategori(char* nvn);
	Kategori* hentAktivUnderkategori();
	void settAktivUnderkategori(Kategori* k);

};


#endif