#if !defined(__KUNDE_H)
#define __KUNDE_H 

#include <fstream>
#include "ListTool2B.h"

using namespace std;

class Kunde : public TextElement {
private:

	char* brukernavn;
	char* passord;
	char* navn;
	char* adresse;
	int   postNr;
	char* postSted;
	char* mail;
	int   antKjop;
	int   antSalg;
	int admin;

public:

	Kunde();
	Kunde(char* bnavn);
	Kunde(char* bnavn, ifstream & inn);
	~Kunde();

	char* hentBrukernavn();
	char* hentPassord();
	void  byttAdmin(); 
	int  hentAdmin();
	void  display();
	void  skrivTilFil(ofstream & ut);

};


#endif