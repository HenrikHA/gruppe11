#if !defined(__KUNDER_H)
#define __KUNDER_H 

#include "ListTool2B.h"
#include "timer4.h"
#include "Kunde.h"

class Kunder {
private:

	List* kunder = new List(Sorted);
	Kunde* aktivKunde;


public:

	Kunder();
	~Kunder();
	
	Kunde* hentAktivKunde();
	List* hentKunder();
	void settAktivKunde(Kunde* k);

};


#endif