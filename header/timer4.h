
#if !defined(__TIMER4_H)
#define  __TIMER4_H

#include <ctime>

class Timer  {
  private:
    tm  tidspunkt;
  public: 
    void hent(int& dag, int& mnd, int& aar, int& time, int& min);
	Timer();
	Timer(tm t);
};

#endif
