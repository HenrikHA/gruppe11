#include "Bud.h"
#include "CONST.h"

Bud::Bud(int d, char* b) : NumElement(d) {

	bud = d;
	budBrukernavn = new char[strlen(b) + 1]; strcpy(budBrukernavn, b);
	settBudTid(hentBud());

}

Bud::Bud(int d, ifstream & inn) : NumElement(d) {

	char buffer[NVNLEN];

	bud = d; 

	inn.getline(buffer, NVNLEN);
	budBrukernavn = new char[strlen(buffer) + 1]; strcpy(budBrukernavn, buffer);

	inn >> budTid;

	inn.ignore(); inn.ignore();

}

void Bud::skrivTilFil(ofstream & ut) {

	ut << bud << "\n";
	ut << budBrukernavn << "\n";
	ut << budTid << "\n\n";

}

void Bud::settBudTid(int d) {

	budTid = d;
}

int Bud::hentBud() {
	return bud;
}

char* Bud::hentBrukernavn() {
	return budBrukernavn;
}