//
//  GLUFUN.cpp
//  ooprogprosjekt
//
//  Created by Simen Bjerkeset Andersen on 22/03/16.
//  Copyright © 2016 Simen Bjerkeset Andersen. All rights reserved.
//

#include "GLOFUN.h"
#include "CONST.h"
#include "Kunde.h"
#include "Kunder.h"
#include "Kategorier.h"
#include "Kategori.h"
#include "Gjenstand.h"
#include "Gjenstander.h"
#include "Bud.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>

using namespace std;

extern char kommando;					// Kommandovalg
extern Kunder* kunderListe;				// Liste over alle kunder
extern Kategorier* kategoriListe;		// Liste over alle kategorier
extern Gjenstander* gjenstandListe;		// Liste over alle gjenstander i minnet
extern int idNummer;					// Neste ledige kategori nummer
extern int idGjenstand;					// Neste ledige gjenstand nummer

// Leser input fra bruker og skriver det til 's'
void lesTekst(const char t[], char s[], const int LEN) {
    
    do  {
        cout << '\t' << t << ": ";      // Skriver ledetekst
        cin.getline(s, LEN);            // Tekstinput fra brukeren
    } while (strlen(s) == 0);
    
}

// Leser tall mello et gitt interval og returnerer dette
int lesTall(const char t[], const int min, const int max) {
    
    int n;
    do  {                               // Skriver ledetekst:
        cout << '\t' << t << " (" << min << '-' << max << "): ";
        cin >> n;   cin.ignore();       // Leser inn ett tall.
    } while(n < min  ||  n > max);      // Sjekker at i lovlig intervall.
    return n;
    
}

// Leser ett tegn, gjør dette til uppercase og returnerer det
char  lesKommando(char t[]) {
    
    char ch;
    cout << '\n' << t << ":  ";   //  Skriver medsendt ledetekst.
    cin >> ch;   cin.ignore();    //  Leser ETT tegn. Forkaster '\n'.
    return (toupper(ch));
    
}

void skrivBrukerMeny() {
    
    cout << "Du er logget inn som Bruker\n"
         << "Følgende kommandoer er mulige:\n"
         << "K S: Se alle egne kjøp og salg\n"
         << "K B: Betale en vunnet auksjon/gjenstand\n"
         << "K F: Gi en annen kunde feedback\n"
         << "A S: Se auksjoner\n"
         << "  A: Se alle auksjonsgjenstandene i aktuall underkategori\n"
         << "  E: Se alle detaljer om en gjenstand\n"
         << "  L: Legg inn en ny auksjonsgjenstand\n"
         << "  B: Legg inn bud på en gjenstand\n"
         << "K U: Logg ut av bruker\n"
         << "  Q: Avslutt\n";
    
}

void skrivAdminMeny() {
    
    cout << "Du er logget inn som Admin\n"
         << "Følgende kommandoer er mulige:\n"
         << "K S: Se alle egne kjøp og salg\n"
         << "K B: Betale en vunnet auksjon/gjenstand\n"
         << "K F: Gi en annen kunde feedback\n"
         << "K E: Endre en annen kundes rettigheter\n"
         << "A H: Legg inn en ny unik hovedkategori\n"
         << "A U: Legg inn en ny underkategodi\n"
         << "A S: Se auksjoner\n"
         << "K U: Logg ut av bruker\n"
         << "  Q: Avslutt\n";
    
}

void skrivGjestMeny() {
    
    cout << "Du er ikke logget inn\n"
         << "Følgende kommandoer er mulige:\n"
         << "K R: Registrer ny kunde\n"
         << "K L: Logg inn en eksiterende kunde\n"
         << "A S: Se auksjoner\n"
         << "  A: Se alle auksjonsgjenstandene i aktuell underkategori\n"
         << "  E: Se alle detaljer om en gjenstand\n"
         << "  Q: Avslutt\n";
    
}

void skrivBrukerAuksjonUnderMeny() {
	cout << "  A: Se alle auksjonsgjenstandene i aktuell underkategori\n"
		 << "  E: Se alle detaljer om en gjenstand\n"
		 << "  L: Legg inn en ny auksjonsgjenstand\n"
		 << "  B: Legg inn bud på en gjenstand\n"
		 << "  Q: Avslutt\n";
}

void skrivGjestAuksjonUnderMeny() {
	cout << "  A: Se alle auksjonsgjenstandene i aktuell underkategori\n"
		 << "  E: Se alle detaljer om en gjenstand\n"
		 << "  Q: Avslutt\n";
}

// Kjøres når 'K' velges i hovedmenyen
void kundeMain(){

	while (kommando != 'Q') {
		switch (kommando) {
		case 'R': nyKunde(); break;				// Oppretter ny kunde
		case 'L': loggInn(); break;				// Logge inn
		case 'U': loggUt();  break;				// Logge ut
		case 'S':  oversiktKjopSalg(); break;	// Oversikt over egne kjøp og salg
		case 'B':  betale(); break;				// Betale gjenstand
		case 'F':  feedback();  break;			// Gi feedback på gjenstand
		case 'E':  endreRettigheter(); break;	// Endre admin rettigheter
		default:   break;
		}
		kommando = lesKommando("Ønske");
	}
	cout << "\n\n";

}


// Kjøres når 'A' velges i hovedmenyen
void auksjonMain() {

	while (kommando != 'Q') {
		switch (kommando) {
		case 'H':  nyHovedkategori();   break;	// Opprette ny hovedkategori
		case 'U':  nyUnderkategori();   break;	// Opprette ny underkategori
		case 'S':  oversiktAuksjoner(); break;	// Videre til aktive auksjoner
		default:   break;
		}
		kommando = lesKommando("Ønske");
	}
	cout << "\n\n";

}

// Oppretter ny kunde
void nyKunde() {

	char buffer[NVNLEN];
	if (!kunderListe->hentAktivKunde()){				// Dersom man ikke allerede
														// er innlogget.
	lesTekst("Skriv inn brukernavn", buffer, NVNLEN);
		if (!finnesKunde(buffer)){						// Sjekker om brukernavn finnes
														// fra før.

		Kunde* k = new Kunde(buffer);					// Oppretter ny kunde

		kunderListe->hentKunder()->add(k);				// Legger kunden til i lista
		}
		else
			cout << "brukernavnet er allerede i bruk!\n";
	}
	else
		cout << "logg ut først!\n";
	skrivTilFil();										// Skriver kunder til fil

}

// Skriver datastrukturer for kategorier og kunder til fil
void skrivTilFil() {

	ofstream utKunder("Kunder.DTA");
	ofstream utKategorier("Kategorier.DTA");
	Kunde* k;
	Kategori* kat;
	int antKunder;
	int antHovedKat;

	// Antall kunder
	antKunder = kunderListe->hentKunder()->noOfElements();

	// Skriver totalt antall kunder til fil
	utKunder << antKunder << "\n\n";

	// For hver kunde...
	for (int i = 1; i <= antKunder; i++) {

		// Henter ut peker til kunde
		k = (Kunde*)kunderListe->hentKunder()->removeNo(i);

		// Legger kunden tilbake i listen
		kunderListe->hentKunder()->add(k);

		// Skriv kundedata til fil
		k->skrivTilFil(utKunder);
	}


	// Skriver siste brukt idnummer til fil
	utKategorier << idNummer << "\n";

	// Skriver siste brukt gjenstandnummer til fil
	utKategorier << idGjenstand << "\n\n";

	// Antall hovedkategorier
	antHovedKat = kategoriListe->hentKategorier()->noOfElements();

	// Skriver antall hovedkat. til fil
	utKategorier << antHovedKat << "\n\n";

	for (int i = 1; i <= antHovedKat; i++) {

		// Henter peker til hovedkat.
		kat = (Kategori*)kategoriListe->hentKategorier()->removeNo(i);

		// Legger hovedkat. tilbake i listen
		kategoriListe->hentKategorier()->add(kat);

		// Skriver kategoridata til fil
		kat->skrivTilFil(utKategorier);

		utKategorier << "\n";
	}

}

// Leser datastruktur fra fil
void lesFraFil() {

	ifstream innKunder("Kunder.DTA");
	ifstream innKategorier("Kategorier.DTA");
	Kunde* k;
	Kategori* kat;
	int antKunder;
	int antHovedKat;
	char buffer[NVNLEN];

	// Leser antall kunder i fil
	innKunder >> antKunder; innKunder.ignore(); innKunder.ignore();

	// Leser inn data om hver kunde fra fil
	for (int i = 1; i <= antKunder; i++) {

		// Leser kundens brukernavn fra fil
		innKunder.getline(buffer, NVNLEN);

		// Leser inn data om kunden fra fil
		k = new Kunde(buffer, innKunder);

		// Legger kunden i kundelisten
		kunderListe->hentKunder()->add(k);
	}


	// Leser inn neste ledige idnummer
	innKategorier >> idNummer; innKategorier.ignore();
	innKategorier >> idGjenstand; innKategorier.ignore(); innKategorier.ignore();

	// Leser inn antall hovedkategorier i filen
	innKategorier >> antHovedKat; innKategorier.ignore(); innKategorier.ignore();

	for (int i = 1; i <= antHovedKat; i++) {

		// Leser hovedkategoriens navn
		innKategorier.getline(buffer, NVNLEN);

		// Oppretter hovedkategori
		kat = new Kategori(buffer, innKategorier);

		innKategorier.ignore();

		// Legger hovedkategorien til listen
		kategoriListe->hentKategorier()->add(kat);

	}




}

// Oppretter ny underkat
void nyUnderkategori() {
    
    char buffer[NVNLEN];
    Kategori* uKat;
    
	// Sjekker om bruker er logget inn
    if (kunderListe->hentAktivKunde()) {		
        
		// Sjekker om bruker er admin
        if (kunderListe->hentAktivKunde()->hentAdmin()) {	
    
			// Leser hovedkategori som underkategori skal tilhøre
            lesTekst("Hovedkategori", buffer, NVNLEN);

			// Dersom hovedkat ikke eksisterer lages denne
			uKat = kategoriListe->hentHovedkategori(buffer);
		
			// Leser underkat navn
			lesTekst("Navn", buffer, NVNLEN);

			// Sjekker om underkat finnes fra før
			if (finnesKategori(buffer, uKat)){

				cout << "Denne underkategorien finnes allerede!\n";

				// Skriver kategori endringer til fil
				skrivTilFil();
			}
    
            else {
    
                // Oppretter underkat
                Kategori* kat = new Kategori(buffer, 0);
				
				// Legger underkat til liste
                uKat->leggeTilUnderkategori(kat);
    
				// Skriver kategori endringer til fil
                skrivTilFil();
			}
            
        }
        
        else
            cout << "Du er ikke logget inn som admin!\n";
        
    }
        
    else
        cout << "Du er ikke logget inn!\n";
    
}

// Oppretter ny hovedkat
void nyHovedkategori() {
    
    char buffer[NVNLEN];
    
	// Sjekker om bruker er innlogget
	if (kunderListe->hentAktivKunde()) {

		// Sjekker om bruker er admin
		if (kunderListe->hentAktivKunde()->hentAdmin()) {

			// Leser navn for ny hovedkat
			lesTekst("Navn", buffer, NVNLEN);

			// Sjekker om hovedkat finnes fra før
			if (finnesKategori(buffer))

				cout << "Denne hovedkategorien finnes allerede!\n";

			else {

				// Oppreter hovedkat
				Kategori* kat = new Kategori(buffer, 1);

				// Legger ny hovedkat i liste
				kategoriListe->hentKategorier()->add(kat);

			}

		} else
			cout << "Du er ikke logget inn som admin!\n";

	} else
		cout << "Du er ikke logget inn!\n";

	// Skriver kategori endringer til fil
	skrivTilFil();
}

void oversiktKjopSalg() {

	// Sjekker om bruker er innlogget
	if (kunderListe->hentAktivKunde()) {
		
		char filK[NVNLEN];
		char filS[NVNLEN];
		char buffer[STRLEN];
		int ant, temp;
		ifstream innK;
		ifstream innS;

		// Henter 'K<brukernavn>.DTA' til innlogget bruker
		hentFilnavnKjoper(filK, kunderListe->hentAktivKunde()->hentBrukernavn());

		// Henter 'S<brukernavn>.DTA' til innlogget bruker
		hentFilnavnSelger(filS, kunderListe->hentAktivKunde()->hentBrukernavn());
		
		// Henter filer
		innK.open(filK, ios::in);
		innK.open(filS, ios::in);

		// Sjekker om fil eksisterer
		if (innK.is_open()){

			// Leser antall gjenstander i fil
			innK >> ant; innK.ignore(); innK.ignore();

			for (int i = 1; i <= ant; i++) {

				// Henter og printer gjenstand id
				innK.getline(buffer, STRLEN);
				cout << "ID: " << buffer << "\n";

				// Henter og printer selgers brukernavn
				innK.getline(buffer, STRLEN);
				cout << "Selgers brukernavn: " << buffer << "\n";

				// Henter og printer betalt status
				innK >> temp; innK.ignore();
				cout << "Betalt: " << temp << "\n";

				// Henter og printer gitt feedback status
				innK >> temp; innK.ignore();
				cout << "Gitt feedback: " << temp << "\n";

				// Henter og printer karakter fått
				innK >> temp; innK.ignore();
				cout << "Karakter mottatt: " << temp << "\n";

				// Henter og printer mottatt feedback
				innK.getline(buffer, STRLEN, '#');
				cout << "Mottatt feedback: " << buffer << "\n";

				// Leser videre i fila
				innK.getline(buffer, STRLEN); innK.ignore();

			}
		}
		else
			cout << "Du har ingen kjøp!\n";

		// Sjekker om fil eksisterer
		if (innS.is_open()) {

			// Leser antall gjenstander i fil
			innS >> ant; innS.ignore(); innS.ignore();

			for (int i = 1; i <= ant; i++) {

				// Henter og printer gjenstand id
				innS.getline(buffer, STRLEN);
				cout << "ID: " << buffer << "\n";

				// Henter og printer kjøpers brukernavn
				innS.getline(buffer, STRLEN);
				cout << "kjøpers brukernavn: " << buffer << "\n";

				// Henter og printer betalt status
				innS >> temp; innS.ignore();
				cout << "Fått betalt: " << temp << "\n";

				// Henter og printer gitt feedback status
				innS >> temp; innS.ignore();
				cout << "Gitt feedback: " << temp << "\n";

				// Henter og printer karakter fått
				innS >> temp; innS.ignore();
				cout << "Karakter mottatt: " << temp << "\n";

				// Henter og printer mottatt feedback
				innS.getline(buffer, STRLEN, '#');
				cout << "Mottatt feedback: " << buffer << "\n";

				// Leser videre i fila
				innS.getline(buffer, STRLEN); innS.ignore();
			}
		}
		else
			cout << "Du har ingen salg!";
	}
	else {
		cout << "Logg inn først!\n";
	}

}

void endreRettigheter() {

	// Dersom bruker er logget inn
	if (kunderListe->hentAktivKunde()) {

		// Dersom bruker er admin
		if (kunderListe->hentAktivKunde()->hentAdmin()) {

			char buffer[NVNLEN];

			lesTekst("Skriv inn brukerens navn", buffer, NVNLEN);

			// Sjekker at admin ikke skriver inn sitt eget brukernavn
			if (strcmp(buffer, kunderListe->hentAktivKunde()->hentBrukernavn()) != 0){

			}
			else
				cout << "Du kan ikke endre dine egne rettigeter!\n";

		}
		else
			cout << "Du må være administrator!\n";

	}
	else
		cout << "Logg inn først!\n";


                skrivTilFil(); 
}

// Sjekker om kategori finnes fra før med medbrakt navn, 'nvn'
// og returnerer eventuelt kategori
Kategori* finnesKategori(char* nvn) {
    
    Kategori* kat;
    
	// Looper igjennom alle katekorier i listen
    for (int i = 1; i <= kategoriListe->hentKategorier()->noOfElements(); i++) {
        kat = (Kategori*) kategoriListe->hentKategorier()->removeNo(i);
    
        kategoriListe->hentKategorier()->add(kat);
        
		// Sjekker om kategori 'i' har samme navn som i parameter
        if (strcmp(nvn, kat->hentNavn()) == 0)
            return kat;
    
    }
    
	// Fant ingen med samme navn
    return nullptr;
        
}

// Sjekker om underkategori navn finnes fra før i hovedkat som medbrakt i parameter
// returnerer eventuelt underkat med samme navn
Kategori* finnesKategori(char* nvn, Kategori* k) {
    
    Kategori* kat;
    
	// Looper igjennom alle underkater i medbrakt hovedkat
    for (int i = 1; i <= k->hentUnderkategorier()->noOfElements(); i++) {
        kat = (Kategori*) k->hentUnderkategorier()->removeNo(i);
        
        k->hentUnderkategorier()->add(kat);
        

		// Sjekker om parameter og underkat har samme navn
        if (strcmp(nvn, kat->hentNavn()) == 0)
            return kat;
        
    }
    
    return nullptr;
   

}

// Sjekker om brukernavn finnes fra før, returnerer eventuelt peker til bruker
Kunde* finnesKunde(char* bNavn){

	Kunde* k;

	for (int i = 1; i <= kunderListe->hentKunder()->noOfElements(); i++){

		// Henter kundepeker og legger den tilbake i listen
		k = (Kunde*)kunderListe->hentKunder()->removeNo(i);
		kunderListe->hentKunder()->add(k);

		// Sjekker like navn
		if (strcmp(bNavn, k->hentBrukernavn()) == 0)
			return k; 
	}
	return nullptr;
}

// Finner gjenstand med gitt 'id' nummer, og returnerer peker til den
Gjenstand* finnGjenstand(int id){

	Gjenstand* g;

	// Looper igjennom alle gjenstander
	for (int i = 1; i <= gjenstandListe->hentListe()->noOfElements(); i++){ 

		// Henter peker til gjenstand og legger den tilbake i listen
		g = (Gjenstand*)gjenstandListe->hentListe()->removeNo(i);
		gjenstandListe->hentListe()->add(g);

		// Sjekker etter lik id
		if (g->hentGjenstandNr() == id)
			return g;
	}
	return nullptr;
}

// Logget ut brker
void loggUt(){

	// Sjekker om bruker er innlogget
	if (kunderListe->hentAktivKunde()){

		// Setter bruker som utlogget
		kunderListe->settAktivKunde(nullptr); 
		cout << "Du er nå logget ut!\n";
	}
	else
		cout << "Du må være innlogget!\n";
}

// Logger inn bruker
void loggInnBruker(char* b, char* p){

	Kunde* k;

	// Looper igjennom alle kunder i listen
	for (int i = 1; i <= kunderListe->hentKunder()->noOfElements(); i++){

		// Henter ut peker til kunde og legger denne tilbake i listen
		k = (Kunde*)kunderListe->hentKunder()->removeNo(i);
		kunderListe->hentKunder()->add(k);

		// Sjeker at brukernavn og passord samsvarer
		if (strcmp(b, k->hentBrukernavn()) == 0){
			if (strcmp(p, k->hentPassord()) == 0)

				// Setter bruker som innlogget
				kunderListe->settAktivKunde(k);
		}
	}
	
	// Printer tilsvarende meny
	if (kunderListe->hentAktivKunde()) {
		if (kunderListe->hentAktivKunde()->hentAdmin() == 0) {
			cout << "Du er nå logget inn som Bruker!\n\n";
			skrivBrukerMeny();
		}
		else {
			skrivAdminMeny();
		}
	}	
	else {
		cout << "Brukernavn eller passord er feil!\n";
	}
}

// Logger inn bruker
void loggInn(){

	// Sjekker at bruker ikke er innlogget
	if (!kunderListe->hentAktivKunde()){
		char brukerNavn[NVNLEN];
		char passord[NVNLEN];

		// Ber om rukernavn og passord
		lesTekst("skriv inn brukernavn", brukerNavn, NVNLEN);
		lesTekst("skriv inn passord", passord, NVNLEN);

		// Sjeker om brukernavn og passord samsvarer
		loggInnBruker(brukerNavn, passord);

	}
	else
		cout << "du er allerede innlogget!\n";
}

// Sender bruker til meny for aktive auksjoner
void oversiktAuksjoner() {

	int valg;
	int antHovedKat = kategoriListe->hentKategorier()->noOfElements();
	int antUnderKat;
	Kategori* tempHKat;
	Kategori* tempUKat;
	char buffer[STRLEN];			

	// Dersom det finnes hovedkater
	if (antHovedKat != 0) {
		cout << "Velg hovedkategori:\n\n";

		// Skriver ut alle hovedkategorier til konsoll
		for (int i = 1; i <= antHovedKat; i++) {

			// Henter ut kategoripeker fra listen
			tempHKat = (Kategori*)kategoriListe->hentKategorier()->removeNo(i);

			// Legger kategorien tilbake i listen
			kategoriListe->hentKategorier()->add(tempHKat);

			// Skriver ut navnet på hovedkategorien til konsoll
			cout << "\t" << "[" << i << "] " << tempHKat->hentNavn() << "\n";

		}

		cout << "\n\n";

		// Ber om å belge hovedkat
		valg = lesTall("Velg hovedkategorinummer", 1, antHovedKat);

		// Henter ut kategoripeker fra listen
		tempHKat = (Kategori*)kategoriListe->hentKategorier()->removeNo(valg);

		// Legger kategorien tilbake i listen
		kategoriListe->hentKategorier()->add(tempHKat);

		// Henter antall underkater
		antUnderKat = tempHKat->hentUnderkategorier()->noOfElements();

		// Sjekker at det finnes underkater
		if (antUnderKat != 0){

			cout << "Velg underkategori:\n\n";



			for (int i = 1; i <= antUnderKat; i++) {
				// Henter ut kategoripeker fra listen
				tempUKat = (Kategori*)tempHKat->hentUnderkategorier()->removeNo(i);

				// Legger kategorien tilbake i listen
				tempHKat->hentUnderkategorier()->add(tempUKat);

				// Skriver ut navnet på underkategorien til konsoll
				cout << "\t" << "[" << i << "] " << tempUKat->hentNavn() << "\n";
			}

			cout << "\n\n";

			valg = lesTall("Velg underkategorinummer", 1, antUnderKat);

			// Henter ut kategoripeker fra listen
			tempUKat = (Kategori*)tempHKat->hentUnderkategorier()->removeNo(valg);

			// Legger kategorien tilbake i listen
			tempHKat->hentUnderkategorier()->add(tempUKat);

			// Setter valgt underkategori som aktiv underkategori
			kategoriListe->settAktivUnderkategori(tempUKat);

			// Fyller gjendstandliste
			lesGjenstandFraFil();

			// Oppdaterer kjøper og selger filer
			oppdater();
			
			// Skriver tilpasset meny
			if (kunderListe->hentAktivKunde())
				skrivBrukerAuksjonUnderMeny();
			else
				skrivGjestAuksjonUnderMeny();

			kommando = lesKommando("Ønske");

			while (kommando != 'Q') {
				switch (kommando) {
				case 'A': oversiktGjenstander();  break;		
				case 'E': oversiktDetaljer(); oppdater(); break;
				case 'L': nyGjenstand(); break;
				case 'B': nyttBud(); break;
				default:
					if (kunderListe->hentAktivKunde())
						skrivBrukerAuksjonUnderMeny();
					else
						skrivGjestAuksjonUnderMeny();
					break;
				}
				kommando = lesKommando("Ønske");
			}
		}
		else
			cout << "Det finnes ingen underkategorier!\n\n";
	}
	else
		cout << "Det finnes ingen hovedkategorier!\n\n";
}

// Funksjon for å legge til ny gjenstand/auksjon
void nyGjenstand() {

	// Sjekker om bruker er innlogger
	if (kunderListe->hentAktivKunde()) {


		// Oppretter ny gjenstand
		Gjenstand* g = new Gjenstand(idGjenstand, kunderListe->hentAktivKunde()->hentBrukernavn());
		gjenstandListe->hentListe()->add(g);
		idGjenstand++;
		skrivGjenstandTilFil();
		skrivTilFil();
	}
	else {
		cout << "Du må være logget inn for å kunne legge ut en gjenstand!\n";
	}
}

// Skriver datastruktur om gjenstander til fil, G<katid>.DTA
void skrivGjenstandTilFil() {

	char buffer[NVNLEN];		// Filnavn for gjenstander
	int antGjenstander = gjenstandListe->hentListe()->noOfElements();
	Gjenstand* tempG;

	hentFilnavn(buffer);

	// Henter fil
	ofstream utG(buffer);

	utG << antGjenstander << "\n\n";
		

	for (int i = 1; i <= antGjenstander; i++) {

		// Henter peker til gjenstand ifra listen
		tempG = (Gjenstand*)gjenstandListe->hentListe()->removeNo(i);

		// Legger gjenstand tilbake i listen
		gjenstandListe->hentListe()->add(tempG);

		tempG->skrivTilFil(utG);


	}

}

// Leser datastruktur om gjenstander fra fil, g<katid>.DTA
void lesGjenstandFraFil() {

	ifstream innG;
	Gjenstand* g;
	int antGjenstander, idNummer;
	char buffer[NVNLEN];
	antGjenstander = gjenstandListe->hentListe()->noOfElements();

	for (int i = 1; i <= antGjenstander; i++){
		gjenstandListe->hentListe()->removeNo(1);
	}

	antGjenstander = gjenstandListe->hentListe()->noOfElements();

	// Henter filnavn på aktiv underkategori
	hentFilnavn(buffer);

	// Åpner fil med gjenstander
	innG.open(buffer);

	// Leser antall gjenstander i fil
	innG >> antGjenstander; innG.ignore(); innG.ignore();

	// Leser inn data om hver gjenstand fra fil
	for (int i = 1; i <= antGjenstander; i++) {

		// Leser gjenstandens id nummer fra fil
		innG >> idNummer; innG.ignore();

		// Leser inn data om gjenstand fra fil
		g = new Gjenstand(idNummer, innG);

		// Legger gjenstand i gjenstandlisten
		gjenstandListe->hentListe()->add(g);

	}
}

// Genererer filnavn for aktiv underkategori
void hentFilnavn(char s[]) {

	char filG[NVNLEN] = "G";		// Filnavn
	char integerTekst[NVNLEN];		// Temp

	// Gjør om 'int' til 'char'
	sprintf(integerTekst, "%d", kategoriListe->hentAktivUnderkategori()->hentNr());

	// Legger til underkategoriens id i filnavnet
	strcat(filG, integerTekst);

	// Legger til filtype i filnavnet
	strcat(filG, ".DTA");

	// Kopierer hele filnavnet til parameter variabel
	strcpy(s, filG);
}

void hentFilnavnKjoper(char s[], const char b[])  {

	char fil[NVNLEN] = "K";		// Filnavn

	strcat(fil, b);

	// Legger til filtype i filnavnet
	strcat(fil, ".DTA");

	// Kopierer hele filnavnet til parameter variabel
	strcpy(s, fil);
}

void hentFilnavnSelger(char s[], const char b[])  {

	char integerTekst[NVNLEN];		// Temp
	char fil[NVNLEN] = "S";		// Filnavn


	strcat(fil, b);

	// Legger til filtype i filnavnet
	strcat(fil, ".DTA");


	// Kopierer hele filnavnet til parameter variabel
	strcpy(s, fil);
}


// Skriver detaljert info om en gjenstand til konsoll
void oversiktDetaljer() {

	Gjenstand* g;

	// Gir bruker valg mellom aktive gjenstander
	g = velgGjenstand("Velg gjenstandsnummer for mer detaljer");

	if (g)
		g->display();


}

// Legge inn nytt bud
void nyttBud() {

	Gjenstand* g;
	Bud* b;
	int bud;
	int antAktive = 0;

	if (kunderListe->hentAktivKunde()) {

		// Looper igjennom alle gjenstander
		for (int i = 1; i <= gjenstandListe->hentListe()->noOfElements(); i++) {

			// Henter ut peker til gjenstand og legger den tilbake i listen
			g = (Gjenstand*)gjenstandListe->hentListe()->removeNo(i);
			gjenstandListe->hentListe()->add(g);

			if (g->hentAktiv() == 1)
				antAktive++;
		}

		// Sjekker at det finnes aktive auksjoner
		if (antAktive != 0) {

			// Bruker velger aktiv auksjon å by på
			g = velgGjenstand("Velg gjenstandnummer for å by på auksjon");

			// Sjekker om det allerede har blitt bydd på
			if (g->hentBud()->noOfElements() == 0) {
				bud = lesTall("Bud", g->hentStartPris(), MAXBUD);
			}

			// Det finnes allerede bud på auksjon
			else {	
				b = (Bud*)g->hentBud()->removeNo(1);
				g->hentBud()->add(b);

				bud = lesTall("Bud", 1000000 - b->hentBud() + g->hentBudOkning(), MAXBUD);
			}

			// Oppdaterer kjøper og selger filer
			oppdater();

			// Sjekker at bruker ikke by på egen auksjon
			if (strcmp(g->hentSelger(), kunderListe->hentAktivKunde()->hentBrukernavn()) != 0) {
				b = new Bud(1000000 - bud, kunderListe->hentAktivKunde()->hentBrukernavn());
				g->hentBud()->add(b);
				skrivGjenstandTilFil();
			}
			else {
				cout << "Du kan ikke by på din egen gjenstand!\n\n";
			}
		}
		else
			cout << "Ingen aktive auksjoner å by på!\n\n";
	}
	else
		cout << "Du må logge inn først!\n\n";
}

Gjenstand* velgGjenstand(char s[]) {

	int antGjenstander = gjenstandListe->hentListe()->noOfElements();
	List* aktiveGjenstander = new List(Sorted);
	int antAktiveGjenstander = aktiveGjenstander->noOfElements();
	int valg;
	Gjenstand* g;
	
	// Tømmer liste over aktive gjenstander
	for (int i = 1; i <= antAktiveGjenstander; i++)
		aktiveGjenstander->removeNo(1);

	cout << "Gjenstander: \n\n";

	// Henter aktive gjenstander
	for (int i = 1; i <= antGjenstander; i++) {
		g = (Gjenstand*)gjenstandListe->hentListe()->removeNo(i);
		gjenstandListe->hentListe()->add(g);
		if (g->hentAktiv() == 1)
			aktiveGjenstander->add(g);
	}

	antAktiveGjenstander = aktiveGjenstander->noOfElements();


	// Skriver aktive auksjner til konsoll
	for (int i = 1; i <= antAktiveGjenstander; i++) {
		g = (Gjenstand*)aktiveGjenstander->removeNo(i);
		aktiveGjenstander->add(g);
		cout << "\t[" << i << "] " << g->hentTittel() << "\n";
	}

	// Ber bruker velge auksjoner dersom de finnes
	if (antAktiveGjenstander != 0) {
		cout << "\n";
		valg = lesTall(s, 1, antAktiveGjenstander);
		cout << "\n";

		g = (Gjenstand*)aktiveGjenstander->removeNo(valg);
		aktiveGjenstander->add(g);

		return g;
	}
	else {
		cout << "Det finnes ingen aktive gjenstander!\n\n";
		return nullptr;
	}
}

// Henter tid i format "ÅÅMMDDTTMM"
int hentTid() {

	int tid, dag, maned, aar, time, minutt;
	Timer t;
	char buffer[3];
	char tall[11] = "";

	t.hent(dag, maned, aar, time, minutt);

	aar -= 100;

	itoa(aar, buffer, 10);
	strcat(tall, buffer);

	if (maned < 10) {
		strcat(tall, "0");
	}

	itoa(maned, buffer, 10);
	strcat(tall, buffer);

	if (dag < 10) {
		strcat(tall, "0");
	}

	itoa(dag, buffer, 10);
	strcat(tall, buffer);

	if (time < 10) {
		strcat(tall, "0");
	}

	itoa(time, buffer, 10);
	strcat(tall, buffer);

	if (minutt < 10) {
		strcat(tall, "0");
	}

	itoa(minutt, buffer, 10);
	strcat(tall, buffer);

	tid = atoi(tall);

	return tid;
}

// Gjør om tid til format "ÅÅMMDDTTMM"
int hentTid(int d, int m, int y, int t, int mi) {

	int tid;
	int dag = d;
	int maned = m;
	int aar = y;
	int time = t;
	int minutt = mi;
	char buffer[5];
	char tall[16] = "";

	itoa(aar, buffer, 10);
	strcat(tall, buffer);

	if (maned < 10) {
		strcat(tall, "0");
	}

	itoa(maned, buffer, 10);
	strcat(tall, buffer);

	if (dag < 10) {
		strcat(tall, "0");
	}

	itoa(dag, buffer, 10);
	strcat(tall, buffer);

	if (time < 10) {
		strcat(tall, "0");
	}

	itoa(time, buffer, 10);
	strcat(tall, buffer);

	if (minutt < 10) {
		strcat(tall, "0");
	}

	itoa(minutt, buffer, 10);
	strcat(tall, buffer);

	tid = atoi(tall);

	return tid;
}

// Displayer tid i format: TT:MM - DD.MM.ÅÅ
void displayTid(int d) {

	char buffer[11];

	itoa(d, buffer, 10);


	cout << buffer[6] << buffer[7] << ":" << buffer[8] << buffer[9] 
		<< " - " << buffer[4] << buffer[5] << "." << buffer[2] << buffer[3]
		<< "." << buffer[0] << buffer[1] << "\n";


}

// Oppdaterer kjøper og selger filer
void oppdater() {

	int antGjenstander = gjenstandListe->hentListe()->noOfElements();
	Gjenstand* g;
	Bud* b;
	Kunde* k;

	for (int i = 1; i <= antGjenstander; i++) {
		g = (Gjenstand*)gjenstandListe->hentListe()->removeNo(i);
		gjenstandListe->hentListe()->add(g);

		// SJekker om auksjon har utgått
		if (hentTid() >= g->hentTidspunktSlutt()) {
			if (g->hentAktiv() == 1) {
				g->settAktiv(0);

				// Henter alle bud på auksjon
				if (g->hentBud()->noOfElements() != 0)  {
					b = (Bud*)g->hentBud()->removeNo(1);
					g->hentBud()->add(b);

					int count;
					char buffer[NVNLEN + 10];
					fstream utKjoper;

					// Skriver til kjøpers fil informasjon om gjenstand
					hentFilnavnKjoper(buffer, b->hentBrukernavn());

					utKjoper.open(buffer, ios::in);

					// Oppretter fil og skriver '0' antall gjenstander
					if (!utKjoper.is_open()) {
						utKjoper.open(buffer, ios::out);
						utKjoper << "0\n\n";
						utKjoper.close();
					}
					utKjoper.close();
					utKjoper.open(buffer, ios::in);
					utKjoper >> count;
					utKjoper.close();

					// Legger til en i totalt antall genstander i fil
					utKjoper.open(buffer, ios::out | ios::in);
					utKjoper << count + 1;
					utKjoper.close();
					utKjoper.open(buffer, ios::app);

					utKjoper << g->hentGjenstandNr() << "\n";

					utKjoper << g->hentSelger() << "\n";

					utKjoper << "0\t0\t0\n";

					for (int j = 1; j <= STRLEN - 1; j++)
						utKjoper << "#";

					utKjoper << "\n\n";

					utKjoper.close();


					// Skriver til selgers fil info om gjenstand
					hentFilnavnSelger(buffer, g->hentSelger());

					utKjoper.open(buffer, ios::in);

					// Oppretter fil og skriver '0' antall gjenstander
					if (!utKjoper.is_open()) {
						utKjoper.open(buffer, ios::out);
						utKjoper << "0\n\n";
						utKjoper.close();
					}
					utKjoper.close();
					utKjoper.open(buffer, ios::in);
					utKjoper >> count;
					utKjoper.close();
					utKjoper.open(buffer, ios::out | ios::in);
					utKjoper << count + 1;
					utKjoper.close();
					utKjoper.open(buffer, ios::app);

					utKjoper << g->hentGjenstandNr() << "\n";

					utKjoper << b->hentBrukernavn() << "\n";

					utKjoper << "0\t0\t0\n";

					for (int j = 1; j <= STRLEN - 1; j++)
						utKjoper << "#";

					utKjoper << "\n\n";


				}
				else {
					// Sletter utgått gjenstand uten bud
					// gjenstandListe->hentListe()->removeNo(i);
				}

			}


		}
			
	}

	skrivGjenstandTilFil();

}

void betale() {

	fstream fil;
	char buffer[NVNLEN];
	char selger[NVNLEN];
	int temp;
	int temp1;
	int count = 0;
	int antVunnet;
	int valg;

	// Sjekker om bruker er innlogget
	if (kunderListe->hentAktivKunde()) {

		// Henter 'K<brukernavn>.DTA' til innlogget bruker
		hentFilnavnKjoper(buffer, kunderListe->hentAktivKunde()->hentBrukernavn());

		// Åpner filen
		fil.open(buffer, ios::in);

		// Sjekker om fil eksisterer
		if (fil.is_open()) {

			fil >> antVunnet; fil.ignore(); fil.ignore();
			int *vunnet = new int[antVunnet + 1];

			// Leser in data fra fil
			for (int i = 0; i < antVunnet; i++) {
				fil >> temp1;
				cout << temp1 << "\n";
				fil.ignore();
				fil.getline(buffer, NVNLEN);
				fil >> temp;
				cout << temp << "\n";
				if (temp == 0)		// Dersom gjenstand ikke er betalt legges det til array
					vunnet[count++] = temp1;
				fil.getline(buffer, NVNLEN);
				fil.getline(buffer, NVNLEN);
				fil.ignore();
			}

			cout << "Velg gjenstandnummer for å betale:\n\n";
			Gjenstand* g;

			for (int i = 0; i < count; i++) {
				g = finnGjenstand(vunnet[i]);

				cout << "\t" << vunnet[i] << "\n";
			}


			cout << "Velg gjenstand ID: ";
			cin >> valg;
			fil.close();
			hentFilnavnKjoper(buffer, kunderListe->hentAktivKunde()->hentBrukernavn());
			fil.open(buffer, ios::out | ios::in);

			sokFil(fil, valg, "1", 'b', selger);

			hentFilnavnSelger(buffer, selger);
			fil.close();
			fil.open(buffer, ios::out | ios::in);

			sokFil(fil, valg, "1", 'b', selger);
			fil.close();
		}
		else
			cout << "Du har ingen kjøp!\n\n";
	}
	else
		cout << "Du må være innlogget!\n\n";
}

// Parametre: filnavn, id å søke etter, tekst å skrive til fil, 
//			  hva skal endres 'betalt' eller 'feedback', henter selger 
void sokFil(fstream & fil, int id, char s[], char v, char t[]) {

	char buffer[STRLEN];
	char inttekst[STRLEN];
	int pos;

	while (strcmp(buffer, itoa(id, inttekst, 10)) != 0) {
		fil.getline(buffer, STRLEN);
	}
	fil.getline(t, NVNLEN);
	if (v == 'g') {				// Gitt feedback
		fil.ignore(); fil.ignore();
	}
	else if (v == 'f') {		// Gir feedback til bruker
		fil.ignore(); fil.ignore(); fil.ignore(); fil.ignore();
	}

	// Lagerer posisjon
	pos = fil.tellg();

	// Endrer tekst i 'pos' til det som er medbrakt i parameter 's'
	fil.seekp(pos, ios::beg);
	if (v != 'q')
		fil << s;

}

// Gir feedback til bruker
void feedback() {

	fstream fil;
	int antGjenstander;
	int temp, valg, karakter;
	int count = 0;
	char buffer[STRLEN];
	char filnavn[NVNLEN];
	char feedback[NVNLEN] = "";
	char selger[NVNLEN];

	// Sjekker om bruker er innlogget
	if (kunderListe->hentAktivKunde()) {

		valg = lesTall("Gi tilbakemelding for kjøp (1) eller salg (2)", 1, 2);

		cout << "Velg gjenstand å gi tilbakemelding til (id):\n\n";

		if (valg == 1) {

			// Henter 'K<brukernavn>.DTA' for innlogget bruker
			hentFilnavnKjoper(filnavn, kunderListe->hentAktivKunde()->hentBrukernavn());

			// Åpner filen for kjøper i lese modus
			fil.open(filnavn, ios::in);

			// Henter ut antall gjenstander i innlogget kjøpers fil
			fil >> antGjenstander; fil.ignore(); fil.ignore();

			for (int i = 1; i <= antGjenstander; i++) {

				// Leser id
				fil.getline(buffer, NVNLEN);

				// Leser selgers brukernavn
				fil.getline(selger, NVNLEN);

				// Hoper over 'kjøpt' verdi samt tabulator
				fil.ignore(); fil.ignore();
				fil >> temp; fil.ignore(); fil.ignore(); fil.ignore();
				if (temp == 0) {
					// Skriver gjenstand id og selger til konsoll
					cout << "\t" << buffer << " - " << selger << "\n";
					count++;
				}

				// Leser kommentar og hoppver over newline
				fil.getline(buffer, STRLEN); fil.ignore();


			}
			fil.close();
			if (count == 0) {
				cout << "Ingen gjenstander å gi tilbakemelding på!\n\n";
			}
			else {

				cout << "Velg ID for å gi tilbakemelding";
				cin >> valg;

				karakter = lesTall("Gi karakter", 1, 6);
				itoa(karakter, buffer, 10);

				strcat(feedback, buffer);
				strcat(feedback, "\n");

				lesTekst("Skriv tilbakemelding", buffer, STRLEN);

				strcat(feedback, buffer);

				// Henter 'K<brukernavn>.DTA' for innlogget bruker
				hentFilnavnKjoper(filnavn, kunderListe->hentAktivKunde()->hentBrukernavn());

				// Åpner fil i 'overskriv' modus
				fil.open(filnavn, ios::out | ios::in);

				// Skriver til innlogget kjøpers fil at en har gitt tilbakemelding
				// samt å hente selgerens brukernavn
				sokFil(fil, valg, "1", 'g', selger);

				fil.close();

				// Henter 'S<brukernavn>.DTA' til selger
				hentFilnavnSelger(filnavn, selger);

				// Åpner fil i 'overskriv' modus
				fil.open(filnavn, ios::out | ios::in);
				cout << feedback << "\n";
				sokFil(fil, valg, feedback, 'f', selger);

				fil.close();
				cout << "Tilbakemelding gitt!\n\n";

			}

			fil.close();
		}
		else {

			// Henter 'S<brukernavn>.DTA' for innlogget bruker
			hentFilnavnSelger(filnavn, kunderListe->hentAktivKunde()->hentBrukernavn());

			// Åpner filen for kjøper i lese modus
			fil.open(filnavn, ios::in);

			// Henter ut antall gjenstander i innlogget kjøpers fil
			fil >> antGjenstander; fil.ignore(); fil.ignore();

			for (int i = 1; i <= antGjenstander; i++) {

				// Leser id
				fil.getline(buffer, NVNLEN);

				// Leser kjøpers brukernavn
				fil.getline(selger, NVNLEN);

				// Hoper over 'kjøpt' verdi samt tabulator
				fil.ignore(); fil.ignore();
				fil >> temp; fil.ignore(); fil.ignore(); fil.ignore();
				if (temp == 0) {
					// Skriver gjenstand id og kjøper til konsoll
					cout << "\t" << buffer << " - " << selger << "\n";
					count++;
				}

				// Leser kommentar og hoppver over newline
				fil.getline(buffer, STRLEN); fil.ignore();


			}
			fil.close();
			if (count == 0) {
				cout << "Ingen gjenstander å gi tilbakemelding på!\n\n";
			}
			else {

				cout << "Velg ID for å gi tilbakemelding";
				cin >> valg;

				karakter = lesTall("Gi karakter", 1, 6);
				itoa(karakter, buffer, 10);

				strcat(feedback, buffer);
				strcat(feedback, "\n");

				lesTekst("Skriv tilbakemelding", buffer, STRLEN);

				strcat(feedback, buffer);

				// Henter 'S<brukernavn>.DTA' for innlogget bruker
				hentFilnavnSelger(filnavn, kunderListe->hentAktivKunde()->hentBrukernavn());

				// Åpner fil i 'overskriv' modus
				fil.open(filnavn, ios::out | ios::in);

				// Skriver til innlogget kjøpers fil at en har gitt tilbakemelding
				// samt å hente selgerens brukernavn
				sokFil(fil, valg, "1", 'g', selger);

				fil.close();

				// Henter 'K<brukernavn>.DTA' til kjøper
				hentFilnavnKjoper(filnavn, selger);

				// Åpner fil i 'overskriv' modus
				fil.open(filnavn, ios::out | ios::in);
				cout << feedback << "\n";
				sokFil(fil, valg, feedback, 'f', selger);

				fil.close();
				cout << "Tilbakemelding gitt!\n\n";

			}

			fil.close();

		}
	}
 else {
	 cout << "Logg inn først!\n\n";
 }

}


// Skriver titler for aktive auksjoner til konsoll
void oversiktGjenstander() {

	int antGjenstander = gjenstandListe->hentListe()->noOfElements();
	Gjenstand* g;
	
	if (antGjenstander != 0) {

		// Sjekker alle gjenstander
		for (int i = 1; i <= antGjenstander; i++) {
			g = (Gjenstand*)gjenstandListe->hentListe()->removeNo(i);
			gjenstandListe->hentListe()->add(g);


			// Printer de til konsoll dersom de er aktive
			if (g->hentAktiv() == 1) {
				cout << "\t" << g->hentTittel() << "\n";
			}

		}

		cout << "\n\n";


	}
	else
		cout << "Det ligger ingen aktive auksjoner ute!\n\n";
}