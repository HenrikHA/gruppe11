#include "Gjenstand.h"
#include "CONST.h"
#include "Bud.h"
#include "GLOFUN.h"
#include "ListTool2B.h"
#include "timer4.h"
#include <ctime>
#include <iostream>

using namespace std;

Gjenstand::Gjenstand() {

}

Gjenstand::Gjenstand(int id, char* selger) : NumElement(id) {

	char buffer[NVNLEN];
	int dag, maned, aar, time, minutt;
	int tdag, tmaned, taar, ttime, tminutt;
	Timer t;
	t.hent(dag, maned, aar, time, minutt);

	// Lagrer unik id til gjenstand
	gjenstandNr = id;

	// Lagrer selgerens brukernavn
	brukernavnSelger = new char[strlen(selger) + 1]; strcpy(brukernavnSelger, selger);

	// Leser inn gjenstand tittel, setter av plass i minnet og lagrer
	lesTekst("Skriv inn gjenstandens tittel", buffer, STRLEN);
	gjenstandTittel = new char[strlen(buffer) + 1]; strcpy(gjenstandTittel, buffer);

	// Leser inn beskrivelse av gjenstand, setter av plass i minnet og lagrer
	lesTekst("Beskrivelse", buffer, STRLEN);
	beskrivelse = new char[strlen(buffer) + 1]; strcpy(beskrivelse, buffer);

	settTidspunktStart(hentTid());
	cout << "Skriv inn sluttdato:\n\n";

	// TidSLutt
	taar = lesTall("Velg �r", aar - 100, aar - 99);

	if (taar == aar - 100)
		tmaned = lesTall("Velg m�ned", maned, 11);
	else
		tmaned = lesTall("Velg m�ned", 0, 11);
	
	if (tmaned == maned) 
		tdag = lesTall("Velg dag", dag, 31);
	else
		tdag = lesTall("Velg dag", 1, 31);
	
	if (tdag == dag)
		ttime = lesTall("Velg time", time, 23);
	else
		ttime = lesTall("Velg time", 0, 23);

	if (ttime == time)
		tminutt = lesTall("Velg minutt", minutt, 59);
	else
		tminutt = lesTall("Velg minutt", 0, 59);

	settTidspunktSlutt(hentTid(tdag, tmaned, taar, ttime, tminutt));
	
	// Leser inn startpris
	startPris = lesTall("Startpris", 1, MAXPRIS);
	gjeldenePris = startPris;

	// Leser inn minimum bud�kning
	budOkning = lesTall("Minimum bud�kning", 1, MAXOKNING);

	// Leser inn porto
	porto = lesTall("Porto", 1, MAXPORTO);

	// Setter gjenstand som aktiv
	aktiv = 1;

	// Setter listetype
	budListe = new List(Sorted);

}

Gjenstand::Gjenstand(int id, ifstream & inn) : NumElement(id) {

	char buffer[STRLEN];
	int dag, maned, aar, time, minutt;
	int antBud;

	// Lagrer gjenstand id
	gjenstandNr = id;

	// Leser selgerens brukernavn
	inn.getline(buffer, STRLEN);
	brukernavnSelger = new char[strlen(buffer) + 1]; strcpy(brukernavnSelger, buffer);

	// Leser gjenstandens tittel
	inn.getline(buffer, STRLEN);
	gjenstandTittel = new char[strlen(buffer) + 1]; strcpy(gjenstandTittel, buffer);

	// Leser beskrivelse
	inn.getline(buffer, STRLEN);
	beskrivelse = new char[strlen(buffer) + 1]; strcpy(beskrivelse, buffer);

	// Leser starttid og lagrer det
	inn >> tidspunktStart; inn.ignore();

	// Leser sluttid og lagrer det
	inn >> tidspunktSlutt; inn.ignore();

	// Leser startpris, minste bud�kning og porto
	inn >> startPris >> gjeldenePris >> budOkning >> porto >> aktiv; inn.ignore();

	// Leser antall bud
	inn >> antBud; inn.ignore(); inn.ignore();

	budListe = new List(Sorted);

	if (antBud != 0) {
		
		Bud* b;
		int budT;

		for (int i = 1; i <= antBud; i++) {
			inn >> budT; inn.ignore();

			b = new Bud(budT, inn);

			budListe->add(b);


		}
		inn.ignore();
	}
	else {
		inn.ignore();
	}

}

int Gjenstand::hentGjenstandNr() {
	return gjenstandNr;
}

char* Gjenstand::hentTittel() {
	return gjenstandTittel;
}


int Gjenstand::hentTidspunktStart() {
	return tidspunktStart;
}

int Gjenstand::hentTidspunktSlutt() {
	return tidspunktSlutt;
}

void Gjenstand::display() {

	cout << "ID: " << gjenstandNr << "\n";
	cout << "Selger: " << brukernavnSelger << "\n";
	cout << "Tittel: " << gjenstandTittel << "\n";
	cout << "Beskrivelse: " << beskrivelse << "\n";

	cout << "Lagt ut: ";
	displayTid(tidspunktStart);

	cout << "Avsluttes: ";
	displayTid(tidspunktSlutt);

	cout << "Startpris: " << startPris << "\n";
	cout << "Gjeldene pris: " << gjeldenePris << "\n";
	cout << "Minimum bud�kning: " << budOkning << "\n";
	cout << "Porto : " << porto << "\n\n";
}



void Gjenstand::settTidspunktStart(int d) {

	tidspunktStart = d;

}

void Gjenstand::settTidspunktSlutt(int d) {
	
	tidspunktSlutt = d;
}


int Gjenstand::hentStartPris() {
	return startPris;
}


int Gjenstand::hentBudOkning() {
	return budOkning;
}

List* Gjenstand::hentBud() {
	return budListe;
}

void Gjenstand::skrivTilFil(ofstream & ut) {

	Bud* b;
	int dag, maned, aar, time, minutt;
	int antBud = budListe->noOfElements();

	ut << gjenstandNr << "\n";
	ut << brukernavnSelger << "\n";
	ut << gjenstandTittel << "\n";
	ut << beskrivelse << "\n";

	ut << tidspunktStart << "\n";

	ut << tidspunktSlutt << "\n";

	ut << startPris << "\t" << gjeldenePris << "\t" << budOkning << "\t" 
		<< porto  << "\t" << aktiv << "\n";


	if (antBud != 0) {

		ut << antBud << "\n\n";

		for (int i = 1; i <= antBud; i++) {

			b = (Bud*)budListe->removeNo(i);
			budListe->add(b);
			b->skrivTilFil(ut);

		}
		ut << "\n";
	}
	else
		ut << "0\n\n\n";

}

char* Gjenstand::hentSelger() {
	return brukernavnSelger;
}

int Gjenstand::hentAktiv() {
	return aktiv;
}

void Gjenstand::settAktiv(int d) {

	aktiv = d;

}