#include "Kategori.h"
#include "CONST.h"
#include <cstring>
#include <fstream>

using namespace std;

extern int idNummer;

// Kategori constructor
Kategori::Kategori() {
    underkategorier = new List(Sorted);
}


// Kategori constructor
Kategori::Kategori(char* nvn, int kat) : TextElement(nvn) {
    navn = new char[strlen(nvn)+1];
    strcpy(navn, nvn);
    nr = idNummer++;
	hovedKat = kat;
    underkategorier = new List(Sorted);
    //underkategorier = NULL;
}

// Kategori deconstructor
Kategori::~Kategori() {

}


// Returnerer navn til kategori
char* Kategori::hentNavn(){
	return navn;
}

// Returnerer kategoriens "skjulte" nummer
int Kategori::hentNr() {
	return nr;
}

// Returnerer, i tall, om kategorien er en hovedkat.
int Kategori::hentHovedKat() {
	return hovedKat;
}

// Returnerer liste over underkategorier
List* Kategori::hentUnderkategorier() {
	return underkategorier;
}

// Skriver kategori info til fil
void Kategori::skrivTilFil(ofstream & ut) {

	Kategori* kat;

	ut << navn << "\n";
	ut << nr << "\n";
	ut << hovedKat << "\n";

	if (hovedKat == 1){
		ut << underkategorier->noOfElements() << "\n\n";

		for (int i = 1; i <= underkategorier->noOfElements(); i++) {

			// Hente ut peker for underkategori
			kat = (Kategori*)underkategorier->removeNo(i);

			// Legger underkategorien tilbake i listen
			underkategorier->add(kat);

			ut << kat->hentNavn() << "\n";
			ut << kat->hentNr() << "\n\n";
		}
	}
}

Kategori::Kategori(char* nvn, ifstream & inn) : TextElement(nvn) {

	char buffer[NVNLEN];
	int antUnderKat;
	Kategori* underKat;

	underkategorier = new List(Sorted);

	// Lagrer navn medbrakt som parameter
	navn = new char[strlen(nvn) + 1]; strcpy(navn, nvn);

	inn >> nr; inn.ignore();

	inn >> hovedKat; inn.ignore();

	inn >> antUnderKat; inn.ignore(); inn.ignore();


	for (int i = 1; i <= antUnderKat; i++) {

		// Leser underkategoriens navn
		inn.getline(buffer, NVNLEN);

		underKat = new Kategori(buffer, 0, inn);

		underkategorier->add(underKat);
	}

}

// Constructor for underkategorier lest fra fil
Kategori::Kategori(char* nvn, int underKat, ifstream & inn) : TextElement(nvn) {

	navn = new char[strlen(nvn) + 1]; strcpy(navn, nvn);

	inn >> nr; inn.ignore(); inn.ignore();
	hovedKat = underKat;  // "0"
	underkategorier = new List(Sorted);

}

// 
void Kategori::leggeTilUnderkategori(Kategori* kat) {
    
    underkategorier->add(kat);
    
}