#include "Kategorier.h"
#include "Kategori.h"
#include "GLOFUN.h"
#include <cstring>
#include <iostream>

using namespace std;

extern Kategorier* kategoriListe;

// kategorier constructor
Kategorier::Kategorier() {

}

// Kategorier deconstructor
Kategorier::~Kategorier() {

}

List* Kategorier::hentKategorier() {
    
    return kategorier;
    
}

Kategori* Kategorier::hentHovedkategori(char* nvn) {
    Kategori* kat;
    
    for (int i = 1; i <= kategorier->noOfElements(); i++) {
        kat = (Kategori*) kategorier->removeNo(i);
        kategorier->add(kat);
    
        if (strcmp(nvn, kat->hentNavn()) == 0)
            return kat;
        
        
    }
    
    cout << "Fant ikke hovedkategorien " << nvn << "!\n";
    cout << "Oppretter hovedkategorien " << nvn << "!";
    
    kat = new Kategori(nvn, 1);
	kategoriListe->hentKategorier()->add(kat);
    return kat;
    
}

Kategori* Kategorier::hentAktivUnderkategori() {

	return aktivUnderkategori;

}

void Kategorier::settAktivUnderkategori(Kategori* k) {

	aktivUnderkategori = k;

}