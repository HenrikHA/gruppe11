#include <iostream>
//#include <fstream>
#include "CONST.h"
#include "GLOFUN.h"
#include "Kunde.h"

using namespace std;

// Kunde construcor
Kunde::Kunde() {

}

// Kunde construcor
Kunde::Kunde(char* bnavn) : TextElement(bnavn) {
	char buffer[NVNLEN];

	// Setter av plass i minnet og lagrer medbrakt brukernavn
	brukernavn = new char[strlen(bnavn) + 1]; strcpy(brukernavn, bnavn);

	// Leser inn passord, setter av plass i minnet og lagrer passord
	lesTekst("Skriv inn passord", buffer, STRLEN);
	passord = new char[strlen(buffer) + 1]; strcpy(passord, buffer);

	// Leser inn navn, setter av plass i minnet og lagrer navn
	lesTekst("Skriv inn navn", buffer, STRLEN);
	navn = new char[strlen(buffer) + 1]; strcpy(navn, buffer);

	// Leser inn adresse, setter av plass i minnet og lagrer adresse
	lesTekst("Skriv inn adresse", buffer, STRLEN);
	adresse = new char[strlen(buffer) + 1]; strcpy(adresse, buffer);

	// Leser inn postnummer
	postNr = lesTall("Skriv inn postnummer", 0, 9999);

	// Leser inn poststed, setter av plass i minnet og lagrer poststed
	lesTekst("Skriv inn poststed", buffer, STRLEN);
	postSted = new char[strlen(buffer) + 1]; strcpy(postSted, buffer);

	// Leser inn mail, setter av plass i minnet og lagrer mail
	lesTekst("Skriv inn mail", buffer, STRLEN);
	mail = new char[strlen(buffer) + 1]; strcpy(mail, buffer);

	// Setter antall kjop og salg til 0
	antKjop = antSalg = 0;

	// Setter admin status til 'false'
	admin = 0;

}

// Kunde constructor, fra fil
Kunde::Kunde(char* bnavn, ifstream & inn) : TextElement(bnavn){

	char buffer[NVNLEN];

	// Lagrer brukernavnet til kunden, medbrakt fra parameter
	brukernavn = new char[strlen(bnavn) + 1]; strcpy(brukernavn, bnavn);

	// Leser og lagrer passordet til kunden 
	inn.getline(buffer, NVNLEN);
	passord = new char[strlen(buffer) + 1]; strcpy(passord, buffer);

	// Leser og lagrer navnet til kunden 
	inn.getline(buffer, NVNLEN);
	navn = new char[strlen(buffer) + 1]; strcpy(navn, buffer);

	// Leser og lagrer kundens adresse
	inn.getline(buffer, NVNLEN);
	adresse = new char[strlen(buffer) + 1]; strcpy(adresse, buffer);

	// Leser og lagrer kundens postnummer
	inn >> postNr; inn.ignore();

	// Leser og lagrer kundens poststed
	inn.getline(buffer, NVNLEN);
	postSted = new char[strlen(buffer) + 1]; strcpy(postSted, buffer);

	// Leser og lagrer kundens email adresse
	inn.getline(buffer, NVNLEN);
	mail = new char[strlen(buffer) + 1]; strcpy(mail, buffer);

	// Leser og lagrer kundens antal kjop, salg og om kunden er admin
	inn >> antKjop >> antSalg >> admin; inn.ignore(); inn.ignore();

}

// Kunde deconstructor
Kunde::~Kunde() {

}


// Returnerer brukernavn som char-pointer
char* Kunde::hentBrukernavn() {
	return brukernavn;
}

// Returnerer passord som char-pointer
char* Kunde::hentPassord() {
	return passord;
}

// Endrer admin bool verdi
void Kunde::byttAdmin() {
	if (admin) {
		admin = 0;
	}
	else
	{
		admin = 1;
	}
}

// Returnerer admin verdi
int Kunde::hentAdmin() {
	return admin;
}

// Displayer info om kunde til konsoll
void Kunde::display() {
	cout << "Info om bruker:";
	cout << "\n\tBrukernavn: \t" << brukernavn;
	cout << "\n\tNavn: \t" << navn;
	cout << "\n\tAdresse: \t" << adresse;
	cout << "\n\tPost Nr.: \t" << postNr;
	cout << "\n\tPoststed: \t" << postSted;
	cout << "\n\tE-mail: \t" << mail;
	cout << "\n\tAntall kj�p: \t" << antKjop;
	cout << "\n\tAntall salg: \t" << antSalg;
	cout << "\n\tAdmin: \t" << admin;
	cout << "\n\n";
}

// Skriver kunde info til fil
void Kunde::skrivTilFil(ofstream & ut) {

	ut << brukernavn << "\n";
	ut << passord << "\n"; 
	ut << navn << "\n";
	ut << adresse << "\n";
	ut << postNr << "\n";
	ut << postSted << "\n";
	ut << mail << "\n";
	ut << antKjop << " " << antSalg << " " << admin << "\n\n";

}