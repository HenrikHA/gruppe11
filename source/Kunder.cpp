#include "Kunder.h"

using namespace std;

// Kunder construcor
Kunder::Kunder() {
	aktivKunde = nullptr;
}

// Kunder deconstructor
Kunder::~Kunder() {

}

// Returnerer peker til innlogget kunde
Kunde* Kunder::hentAktivKunde() {
	return aktivKunde;
}

// Returnerer liste over alle kunder
List* Kunder::hentKunder() {
	return kunder;
}

// Setter aktiv kunde til medbrakt kunde peker
void Kunder::settAktivKunde(Kunde* k) {
	aktivKunde = k;
}