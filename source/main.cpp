//
//  main.cpp
//  ooprogprosjekt
//
//  Created by Simen Bjerkeset Andersen on 22/03/16.
//  Copyright © 2016 Simen Bjerkeset Andersen. All rights reserved.
//

#include <iostream>
#include "GLOFUN.h"
#include "Kunder.h"
#include "Kunde.h"
#include "Kategorier.h"
#include "Kategori.h"
#include "Gjenstander.h"

using namespace std;

char kommando;

int idNummer    = 1000;
int idGjenstand = 10000;

Kunder* kunderListe         = new Kunder();
Kategorier* kategoriListe   = new Kategorier();
Gjenstander* gjenstandListe = new Gjenstander();


int main() {

    lesFraFil();
    skrivGjestMeny();
    
    kommando = lesKommando("Ønske");
    
    while (kommando != 'Q') {
        switch (kommando) {
			case 'K': kundeMain();  break;
			case 'A': auksjonMain(); break;
			case 'T': oppdater(); break;	// Fjernes før levering
			default:  skrivGjestMeny(); break;
        }
        kommando = lesKommando("Ønske");
    }
    cout << "\n\n";
    return 0;
}

