
#include <ctime>
#include "timer4.h"      //  Timer-klassen


void Timer::hent(int& dag, int& mnd, int& aar, int& tim, int& min)  {
								//  Evt. p� Linux/Mac:
	time_t tid; // __time64_t  tid;					//  time_t tid;
	//tid = time(NULL);
    time( &tid ); // _time64(&tid);						//  tid = time(NULL);  /  time( &tid );
	tidspunkt = *localtime(&tid); // _localtime64_s(&tidspunkt, &tid);	//  tidspunkt = localtime(&tid);
	dag = tidspunkt.tm_mday;
	mnd = tidspunkt.tm_mon;
	aar = tidspunkt.tm_year;
	tim = tidspunkt.tm_hour;
	min = tidspunkt.tm_min;
}

Timer::Timer(tm t) {
	tidspunkt = t;
}

Timer::Timer() {
	time_t tid;
	time(&tid);
	tidspunkt = *localtime(&tid);
}